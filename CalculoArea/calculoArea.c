#include <stdio.h>

int main()
{
    float l1, l2, area;

    printf("Escreva o primeiro lado = ");
    scanf("%f", &l1);
    printf("Escreva o segundo lado = ");
    scanf("%f", &l2);

    area = l1 * l2;

    printf("Area = %f m2 \n", area);

    if (area > 10)
        printf("Area maior que 10m2");
    else
        printf("Area menor que 10m2");
}